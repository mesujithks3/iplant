# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import matplotlib.pyplot as plt
import numpy as np
import argparse
import imutils
import copy
import os
import cv2
  
for img in os.listdir("test"):
	# load the image
	image = cv2.imread("test/"+img)
	orig = image.copy()

	# pre-process the image for classification
	image = cv2.resize(image, (100, 100))
	image = image.astype("float") / 255.0
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)

	# load the trained convolutional neural network
	print("[INFO] Loading network...")
	model = load_model("iplant.modelnew.h5")

	# classify the input image
	img_class = model.predict_classes(image)
	pred = model.predict(image)
	accs = []
	classname = img_class[0]
	for index, value in enumerate(pred[0]):
            accs.append(round(value * 100,3))
            
	print(img_class)
	print(accs)
	print(accs[classname])
	
	print("[RESULT] Image: "+str(img)+" Class: "+str(classname))
	
	plt.imshow(orig)
	plt.title("Image: "+str(img)+" Class: "+str(classname))
	plt.show()

#TODO://FIND np.argmax(result) FOR EACH DISEASE AND VERIFY
'''
d={}
for index, value in enumerate(result[0]):
		d[data[index]] = value*100

print(d)
print(data[np.argmax(result)])'''

