Always consider an integrated approach with preventive measures together with biological treatments if available. Chemical treatment of viral diseases is not possible.
