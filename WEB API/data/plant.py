import os
import glob
import json

DIR = ['symptom/','Trigger/','Biological_Control/','Chemical_Control/','Preventive_Measures/']
head = ['symptom','trigger','bcontrol','ccnotrol','pmeasures','names']
names = []
data =[]
dic = {}

files = glob.glob("data/symptom/*.txt")

for name in files:
	name = name.split("/")[2]
	sp = name.split("_")
	sp[len(sp)-1] = sp[len(sp)-1].split(".")[0]
	n = ""
	for w in range(1,len(sp)):
    		n = n + sp[w] + " "
    	names.append(n)
    	
for i in range(len(files)):
	files[i] = files[i].split("/")[2]

for i in range(len(files)):
	dic['names'] = names[i]
	for j in range(len(DIR)):
		f = open(os.path.join(DIR[j],files[i]),"r")
		dic[head[j]] = f.read()
	data.append(dic)

d = json.dumps(data)
print d

