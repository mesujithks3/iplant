from flask import Flask, render_template, request, jsonify
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from keras import backend as K
from werkzeug.utils import secure_filename
import tensorflow as tf
import base64
from PIL import Image
from io import BytesIO
import json
import glob
import codecs
import numpy as np
import cv2
import os
import matplotlib.pyplot as plt
import imutils

app = Flask(__name__)

global model

verify_dir = 'uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
IMG_SIZE = 100
disease = {0:"Bacterial", 1:"Healthy", 2:"Late Blight", 3:"Viral"}
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    
def get_data(DIR,name):
	try:
		dic = ""
		f = open(os.path.join("data/disease/"+DIR,name),"r")
		dic = f.read()
		f.close()
		return dic
	except Exception:
		return "Server Erorr!"

def analysis(img):
    global model
    # load the trained convolutional neural network
    print("[INFO] Loading network...")
    model = load_model("iplant.modelnew.h5")
    path = os.path.join(verify_dir, img)
    # load the image
    image = cv2.imread(path)
    # pre-process the image for classification
    image = cv2.resize(image, (IMG_SIZE, IMG_SIZE))
    image = image.astype("float") / 255.0
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    
    img_class = model.predict_classes(image)
    pred = model.predict(image)
   
    accs = []
    for index, value in enumerate(pred[0]):
    	accs.append(round(value * 100,3))
    classname = img_class[0]
  
    print("[RESULT] Class: "+disease[classname])
   
    result = {}
    result['status'] = "Healthy"
    result['title'] = disease[classname]
    result['accuracy'] = accs[classname]
    
    if "Healthy" not in disease[classname]:
    	result['status'] = "Unhealthy"
    	result['disease'] = disease[classname]
    	result['bcontrol'] = get_data("Biological_control",disease[classname].replace(" ","_")+".txt")
    	result['ccnotrol'] = get_data("Chemical_control",disease[classname].replace(" ","_")+".txt")
    	result['symptom'] = get_data("Symptom",disease[classname].replace(" ","_")+".txt")
    	result['trigger'] = get_data("Trigger",disease[classname].replace(" ","_")+".txt")
    	result['pmeasures'] = get_data("Preventive_measures",disease[classname].replace(" ","_")+".txt")
    	
    K.clear_session()
    
    return result


@app.route('/API/postImage', methods=['GET', 'POST'])
def get_image():
    
    result = {}

    if request.method == 'POST':
        # check if the post request has the file part 
        if 'image' not in request.files: 
        	result['status'] = "Erorr"
        	result['message'] = "Invalid image"
        	return jsonify(result) 
        file = request.files['image'] 
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(verify_dir, filename))
            return jsonify(analysis(filename))
    return "LOL NO"


@app.route('/')
def homepage():
    name = 'img.jpg'
    # image = pre_process('testpicture/'+name)
    res = analysis(name)
    return render_template('index.html', plant_name=res['title'], treatment_technique=res["symptom"], plant_img=name)



@app.route('/API/Library')
def api_library():
	DIR = ['symptom/','Trigger/','Biological_Control/','Chemical_Control/','Preventive_Measures/']
	head = ['symptom','trigger','bcontrol','ccnotrol','pmeasures','image','name']
	type_dic = {'F': 'Fungus', 'V': 'Virus', 'M': 'Mite', 'B': 'Bacteria', 'I': 'Insect', 'D': 'Deficientcy'}
	names = []
	types = []
	data =[]
	dic = {}

	files = glob.glob("data/symptom/*.txt")

	for name in files:
		name = name.split("/")[2]
		sp = name.split("_")
		sp[len(sp)-1] = sp[len(sp)-1].split(".")[0]
		n = ""
		for w in range(1,len(sp)):
				n = n + sp[w] + " "
		names.append(n)
		types.append(type_dic[sp[0]])
			
	for i in range(len(files)):
		files[i] = files[i].split("/")[2]

	for i in range(len(files)):
		dic = {}
		dic['name'] = names[i]
		dic['image'] = "/static/Images/"+files[i].split(".")[0]+".jpg"
		dic['type'] = types[i]
		for j in range(len(DIR)):
			f = open(os.path.join("data/"+DIR[j],files[i]),"r")
			dic[head[j]] = f.read()
			f.close()
		data.append(dic)

	return jsonify(data)

if __name__ == '__main__':
    app.run()
