package com.targaryen.iplant.Utility;

public class Config {
    // File upload url (replace the ip with your server address)
    public static final String BASE_URL = "http://192.168.43.50:5000";
    public static final String FILE_UPLOAD_URL = BASE_URL + "/API/postImage";
    public static final String LIBRARY_DATA_URL = BASE_URL + "/API/Library";
    public static final String ICON_URL = "http://openweathermap.org/img/w/";
}