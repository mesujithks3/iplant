package com.targaryen.iplant.model;

import java.io.Serializable;

public class Library implements Serializable {

    String name;
    String image;
    String symptom;
    String trigger;
    String bcontrol;
    String ccnotrol;
    String pmeasures;
    String type;

    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String price) {
        this.symptom = price;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String price) {
        this.trigger = price;
    }

    public String getBcontrol() {
        return bcontrol;
    }

    public void setBcontrol(String price) {
        this.bcontrol = price;
    }

    public String getCcnotrol() {
        return ccnotrol;
    }

    public void setCcnotrol(String price) {
        this.ccnotrol = price;
    }

    public String getPmeasures() {
        return pmeasures;
    }

    public void setPmeasures(String price) {
        this.pmeasures = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String price) {
        this.type = price;
    }
}
