package com.targaryen.iplant.Activity;

import android.Manifest;
import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.R;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class MainIntroActivity extends IntroActivity{
    @Override protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Add slides, edit configuration...
        addSlide(new SimpleSlide.Builder()
                .title("iPlant")
                .description("Testing")
                .image(R.drawable.abc_ic_clear_material)
                .background(R.color.background_material_light)
                .backgroundDark(R.color.background_material_dark)
                .permissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
                .scrollable(false)
                .build());
    }
}