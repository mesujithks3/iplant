package com.targaryen.iplant.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.targaryen.iplant.Activity.HistoryActivity;
import com.targaryen.iplant.Activity.MainActivity;
import com.targaryen.iplant.R;
import com.targaryen.iplant.Utility.DatabaseHelper;
import com.targaryen.iplant.Utility.ImageHelper;

import java.io.ByteArrayInputStream;
import java.net.InetAddress;

public class HomeFragment extends Fragment {
    View view;
    private static final String TAG = MainActivity.class.getSimpleName();
    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int REQUEST_WRITE_STORAGE_REQUEST_CODE = 101;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private Uri fileUri; // file url to store image

    private DatabaseHelper databaseHelper;
    private ImageView imageView;
    private TextView title, acc;
    private RelativeLayout relativeLayout;
    private CardView cardView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        databaseHelper = new DatabaseHelper(getActivity());
        relativeLayout = view.findViewById(R.id.rl_result_card);
        imageView = view.findViewById(R.id.iv_last_image);
        title = view.findViewById(R.id.tv_rr_title);
        acc = view.findViewById(R.id.tv_rr_acc);
        if (databaseHelper.getImageCount() > 0)
            setUpImage();
        else relativeLayout.setVisibility(View.GONE);

        //if(isInternetAvailable()){

        // }else showToast("Internet Not Available!");

        return view;
    }


    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");
        } catch (Exception e) {
            return false;
        }
    }

    private void setUpImage() {
        Log.d(TAG, "Decoding bytes");
        cardView = view.findViewById(R.id.rslt_card);
        ImageHelper image = databaseHelper.getLastImage();
        byte[] bytes = image.getImageByteArray();
        if (bytes != null && bytes.length != 0)
            imageView.setImageBitmap(ByteArrayToBitmap(bytes));
        else Toast.makeText(getActivity(), "Error in reading image!", Toast.LENGTH_SHORT).show();
        title.setText(image.getTitle());
        acc.setText("Accuracy: " + image.getAcc());
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                startActivity(intent);
            }
        });
    }

    public Bitmap ByteArrayToBitmap(byte[] byteArray) {
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
        Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
        return bitmap;
    }

    /**
     * Checking device has camera hardware or not
     */
    private boolean isDeviceSupportCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /**
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // get the file url
//        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

}