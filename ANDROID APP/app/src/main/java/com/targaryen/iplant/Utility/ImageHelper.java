package com.targaryen.iplant.Utility;

public class ImageHelper {

    private String imageId;
    private byte[] imageByteArray;
    private String title;
    private String acc;
    private String time_stamp;

    public String getImageId() {
        return imageId;
    }
    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
    public byte[] getImageByteArray() {
        return imageByteArray;
    }
    public void setImageByteArray(byte[] imageByteArray) {
        this.imageByteArray = imageByteArray;
    }
    public String getTimestamp() {
        return time_stamp;
    }
    public void setTimestamp(String imageId) {
        this.time_stamp = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String imageId) {
        this.title = imageId;
    }

    public String getAcc() {
        return acc;
    }

    public void setAcc(String imageId) {
        this.acc = imageId;
    }

}