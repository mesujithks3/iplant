package com.targaryen.iplant.Utility;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Randula.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private Context context;
    private final String TAG = "DatabaseHelperClass";
    private static final int databaseVersion = 1;
    private static final String databaseName = "dbTest";
    private static final String TABLE_IMAGE = "ImageTable";

    // Image Table Columns names
    private static final String COL_ID = "col_id";
    private static final String IMAGE_ID = "image_id";
    private static final String IMAGE_BITMAP = "image_bitmap";
    private static final String TITLE = "title";
    private static final String ACCURACY = "accuracy";
    private static final String TIME_STAMP = "time_stamp";

    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_IMAGE_TABLE = "CREATE TABLE " + TABLE_IMAGE + "("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + IMAGE_ID + " TEXT,"
                + IMAGE_BITMAP + " TEXT,"
                + TITLE + " TEXT,"
                + ACCURACY + " TEXT,"
                + TIME_STAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL)";
        sqLiteDatabase.execSQL(CREATE_IMAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE);
        onCreate(sqLiteDatabase);
    }

    public void insetImage(Uri image_uri, String imageId, String title, String acc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IMAGE_ID, imageId);
        values.put(IMAGE_BITMAP, convertImageToByte(image_uri));
        values.put(TITLE, title);
        values.put(ACCURACY, acc + "%");
        db.insert(TABLE_IMAGE, null, values);
        db.close();
        //Toast.makeText(context, "Inserted to DB", Toast.LENGTH_LONG).show();

    }

    public ImageHelper getImage(String imageId) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor2 = db.query(TABLE_IMAGE,
                new String[]{COL_ID, IMAGE_ID, IMAGE_BITMAP}, IMAGE_ID
                        + " LIKE '" + imageId + "%'", null, null, null, null);
        ImageHelper imageHelper = new ImageHelper();

        if (cursor2.moveToFirst()) {
            do {
                imageHelper.setImageId(cursor2.getString(1));
                imageHelper.setImageByteArray(cursor2.getBlob(2));
            } while (cursor2.moveToNext());
        }

        cursor2.close();
        db.close();
        return imageHelper;
    }

    public List<ImageHelper> getAllImages() {

        List<ImageHelper> images = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGE + " ORDER BY " + TIME_STAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ImageHelper imageHelper = new ImageHelper();
                imageHelper.setImageId(cursor.getString(cursor.getColumnIndex(IMAGE_ID)));
                imageHelper.setImageByteArray(cursor.getBlob(cursor.getColumnIndex(IMAGE_BITMAP)));
                imageHelper.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                imageHelper.setAcc(cursor.getString(cursor.getColumnIndex(ACCURACY)));
                imageHelper.setTimestamp(cursor.getString(cursor.getColumnIndex(TIME_STAMP)));
                images.add(imageHelper);

            } while (cursor.moveToNext());

        }
        // close db connection
        db.close();
        // return notes list
        return images;
    }

    public ImageHelper getLastImage() {
        String selectQuery = "SELECT  * FROM " + TABLE_IMAGE + " ORDER BY " + TIME_STAMP + " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ImageHelper imageHelper = new ImageHelper();
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                imageHelper.setImageId(cursor.getString(cursor.getColumnIndex(IMAGE_ID)));
                imageHelper.setImageByteArray(cursor.getBlob(cursor.getColumnIndex(IMAGE_BITMAP)));
                imageHelper.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                imageHelper.setAcc(cursor.getString(cursor.getColumnIndex(ACCURACY)));
                imageHelper.setTimestamp(cursor.getString(cursor.getColumnIndex(TIME_STAMP)));
            } while (cursor.moveToNext());

        }
        // close db connection
        db.close();
        return imageHelper;
    }

    public int getImageCount() {
        String countQuery = "SELECT  * FROM " + TABLE_IMAGE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        // return count
        return count;
    }

    public byte[] convertImageToByte(Uri uri) {
        byte[] data = null;
        try {
            ContentResolver cr = context.getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }
}