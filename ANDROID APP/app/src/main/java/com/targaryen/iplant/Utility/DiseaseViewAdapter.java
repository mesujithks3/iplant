package com.targaryen.iplant.Utility;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.targaryen.iplant.R;

import java.util.ArrayList;

import at.blogc.android.views.ExpandableTextView;

public class DiseaseViewAdapter extends RecyclerView.Adapter<DiseaseViewAdapter.ViewHolder> {


    // we define a list from the DevelopersList java class

    private ArrayList<String> contents;
    private Context mcontext;
    private String[] names= {"Symptoms","Trigger","Preventive Measure","Bacterial Control","Chemical Control"};

    public DiseaseViewAdapter(ArrayList<String> list, Context context) {

        // generate constructors to initialise the List and Context objects

        this.contents = list;
        this.mcontext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.disease_view_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views
        holder.text_name.setText(names[position]);
        holder.expandableTextView.setText(contents.get(position));
       holder.expandableTextView.setAnimationDuration(750L);

        // set interpolators for both expanding and collapsing animations
       holder.expandableTextView.setInterpolator(new OvershootInterpolator());

// or set them separately
        holder.expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        holder.expandableTextView.setCollapseInterpolator(new OvershootInterpolator());

// toggle the ExpandableTextView
        holder.buttonToggle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(final View v)
            {
                holder.buttonToggle.setBackgroundResource(holder.expandableTextView.isExpanded() ? R.drawable.expand :R.drawable.collapse );
                holder.expandableTextView.toggle();
            }
        });
    }

    @Override

    //return the size of the listItems (developersList)

    public int getItemCount() {
        return contents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

        // define the View objects
        public ExpandableTextView expandableTextView;
        private TextView text_name;
        private Button buttonToggle;

        public ViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects

           expandableTextView= (ExpandableTextView) itemView.findViewById(R.id.expandableTextView);
           buttonToggle = (Button) itemView.findViewById(R.id.button_toggle);
            text_name = (TextView)itemView.findViewById(R.id.item_name);
        }

    }
}
