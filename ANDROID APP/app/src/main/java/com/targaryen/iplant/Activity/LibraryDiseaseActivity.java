package com.targaryen.iplant.Activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.targaryen.iplant.R;
import com.targaryen.iplant.Utility.Config;
import com.targaryen.iplant.Utility.DiseaseViewAdapter;
import com.targaryen.iplant.model.Library;

import java.util.ArrayList;

public class LibraryDiseaseActivity extends AppCompatActivity {

    Library library;
    RecyclerView recyclerView;
    ArrayList<String> arrayList;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_disease);
        library = (Library) getIntent().getSerializableExtra("serialize_data");
        CollapsingToolbarLayout ctl = findViewById(R.id.toolbar_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(library.getName());
        ctl.setTitle(library.getName());
        ImageView bgImg = findViewById(R.id.bgImage);
        Glide.with(this)
                .load(Config.BASE_URL + library.getImage())
                .into(bgImg);

        recyclerView = findViewById(R.id.rv_library_disease);
        arrayList = new ArrayList<>();
        arrayList.add(library.getSymptom());
        arrayList.add(library.getTrigger());
        arrayList.add(library.getPmeasures());
        arrayList.add(library.getBcontrol());
        arrayList.add(library.getCcnotrol());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(LibraryDiseaseActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new DiseaseViewAdapter(arrayList, LibraryDiseaseActivity.this);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

    }
}
