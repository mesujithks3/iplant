package com.targaryen.iplant.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.targaryen.iplant.R;
import com.targaryen.iplant.Utility.DatabaseHelper;
import com.targaryen.iplant.Utility.ImageHelper;

import java.io.ByteArrayInputStream;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<ImageHelper> itemsList;
    private HistoryActivity.HistoryAdapter mAdapter;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar_history);
        setSupportActionBar(toolbar);
        toolbar.setTitle("History");
        databaseHelper = new DatabaseHelper(this);
        recyclerView = findViewById(R.id.rv_history);
        itemsList = databaseHelper.getAllImages();
        mAdapter = new HistoryActivity.HistoryAdapter(this, itemsList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }

    class HistoryAdapter extends RecyclerView.Adapter<HistoryActivity.HistoryAdapter.MyViewHolder> {

        private Context context;
        private List<ImageHelper> libraryList;

        public HistoryAdapter(Context context, List<ImageHelper> libraryList) {
            this.context = context;
            this.libraryList = libraryList;
        }

        @Override
        public void onBindViewHolder(HistoryActivity.HistoryAdapter.MyViewHolder holder, final int position) {
            final ImageHelper library = libraryList.get(position);
            holder.t1.setText(library.getTimestamp());
            holder.title.setText(library.getTitle());
            holder.acc.setText("Accuracy: " + library.getAcc());
            byte[] bytes = library.getImageByteArray();
            if (bytes != null && bytes.length != 0)
                holder.thumbnail.setImageBitmap(ByteArrayToBitmap(bytes));
            else
                Toast.makeText(HistoryActivity.this, "Error in reading image!", Toast.LENGTH_SHORT).show();

        }

        public Bitmap ByteArrayToBitmap(byte[] byteArray) {
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(byteArray);
            Bitmap bitmap = BitmapFactory.decodeStream(arrayInputStream);
            return bitmap;
        }

        @Override
        public HistoryActivity.HistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recent_result_card, parent, false);

            return new HistoryActivity.HistoryAdapter.MyViewHolder(itemView);
        }

        @Override
        public int getItemCount() {
            return libraryList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title, acc, t1;
            public ImageView thumbnail;


            public MyViewHolder(View view) {
                super(view);
                t1 = view.findViewById(R.id.titile_tv);
                title = view.findViewById(R.id.tv_rr_title);
                acc = view.findViewById(R.id.tv_rr_acc);
                thumbnail = view.findViewById(R.id.iv_last_image);
            }
        }
    }
}
