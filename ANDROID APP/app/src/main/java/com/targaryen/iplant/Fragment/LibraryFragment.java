package com.targaryen.iplant.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.targaryen.iplant.Activity.LibraryDiseaseActivity;
import com.targaryen.iplant.R;
import com.targaryen.iplant.Utility.Config;
import com.targaryen.iplant.model.Library;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class LibraryFragment extends Fragment {

    private static final String TAG = LibraryFragment.class.getSimpleName();

    public View view;
    private RecyclerView recyclerView;
    private List<Library> itemsList;
    private LibraryAdapter mAdapter;

    public LibraryFragment() {
        // Required empty public constructor
    }

    public static LibraryFragment newInstance(String param1, String param2) {
        LibraryFragment fragment = new LibraryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_library, null);

        recyclerView = view.findViewById(R.id.rv_library);
        itemsList = new ArrayList<>();
        mAdapter = new LibraryAdapter(getActivity(), itemsList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);

        try {
            loadUrlData();
        }catch (Exception e){
            Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
        }

        return view;

    }





    private void loadUrlData() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                Config.LIBRARY_DATA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONArray array = new JSONArray(response);

                    List<Library> items = new Gson().fromJson(array.toString(), new TypeToken<List<Library>>() {
                    }.getType());

                    itemsList.clear();
                    itemsList.addAll(items);

                    // refreshing recycler view
                    mAdapter.notifyDataSetChanged();

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.MyViewHolder> {

        private Context context;
        private List<Library> libraryList;

        public LibraryAdapter(Context context, List<Library> libraryList) {
            this.context = context;
            this.libraryList = libraryList;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final Library library = libraryList.get(position);
            holder.name.setText(library.getName());
            holder.type.setText(library.getType());
            Glide.with(context)
                    .load(Config.BASE_URL + library.getImage())
                    .into(holder.thumbnail);
            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), LibraryDiseaseActivity.class);
                    intent.putExtra("serialize_data", library);
                    startActivity(intent);
                }
            });

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public int getItemCount() {
            return libraryList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name, type;
            public ImageView thumbnail;
            public LinearLayout layout;


            public MyViewHolder(View view) {
                super(view);
                name = view.findViewById(R.id.lib_name);
                type = view.findViewById(R.id.lib_type);
                thumbnail = view.findViewById(R.id.lib_img);
                layout = view.findViewById(R.id.lib_row);

            }


        }
    }
}