package com.targaryen.iplant.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.targaryen.iplant.R;
import com.targaryen.iplant.Utility.DatabaseHelper;
import com.targaryen.iplant.Utility.DiseaseViewAdapter;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class ServerResult extends AppCompatActivity {

    TextView disease,accuracy,status;
    RecyclerView result_recycler;
    ArrayList<String> arrayList;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    public static final String IMAGE_ID = "IMG_ID";
    private final String TAG = "ServerResult";

    private DatabaseHelper databaseHelper;
    private ImageView dbImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_result);
        disease = (TextView)findViewById(R.id.disease);
        status = (TextView)findViewById(R.id.status);
        accuracy = (TextView)findViewById(R.id.accuracy);

        result_recycler=(RecyclerView)findViewById(R.id.result_recycler);
        arrayList = new ArrayList<>();

        Intent intent = getIntent();
        String path = intent.getStringExtra("file_uri");
        Uri image_uri = Uri.fromFile(new File(path));
        ImageView imageView = findViewById(R.id.iv_result);
        imageView.setImageURI(image_uri);


        try{

            JSONObject jsonObject = new JSONObject(intent.getStringExtra("jsonResult"));

            databaseHelper = new DatabaseHelper(this);
            databaseHelper.insetImage(image_uri, image_uri.getPath(), jsonObject.getString("title"), jsonObject.getString("accuracy"));

            if (jsonObject.has("title")) {
                disease.setText(jsonObject.getString("title"));
            }
            if(jsonObject.has("accuracy")){
                accuracy.setText(jsonObject.getString("accuracy"));
            }
            if(jsonObject.has("status")){
                status.setText(jsonObject.getString("status"));
            }


            if(jsonObject.has("symptom")){
                arrayList.add(jsonObject.getString("symptom"));
            }else{
                arrayList.add("no symptom");
            }

            if(jsonObject.has("trigger")){
                arrayList.add(jsonObject.getString("trigger"));
            }else{
                arrayList.add("no trigger");
            }

            if (jsonObject.has("pmeasures")) {
                arrayList.add(jsonObject.getString("pmeasures"));
            }else{
                arrayList.add("no preventive measure");
            }

            if(jsonObject.has("bcontrol")){
                arrayList.add(jsonObject.getString("bcontrol"));
            }else{
                arrayList.add("no bacterial control");
            }

            if(jsonObject.has("ccnotrol")){
                arrayList.add(jsonObject.getString("ccnotrol"));
            }else{
                arrayList.add("no c control");
            }

            layoutManager = new LinearLayoutManager(ServerResult.this);
            result_recycler.setLayoutManager(layoutManager);
            adapter = new DiseaseViewAdapter(arrayList,ServerResult.this);
            result_recycler.setAdapter(adapter);

        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        super.onBackPressed();
    }
}
